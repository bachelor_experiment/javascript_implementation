var fileHandler = require('./FileHandler')
var insertionSort = require('./SortingAlgorithms/Insertionsort')
var selectionSort = require('./SortingAlgorithms/Selectionsort')
var quickSort = require('./SortingAlgorithms/Quicksort')
var mergeSort = require('./SortingAlgorithms/Mergesort')

// # CHANGE BELOW HERE #
//Change input to import correct testdata.
var unsortedArray = fileHandler.readTestData("testInt_1.txt");
console.log("# Sorting has started #");

const startTime = process.hrtime.bigint();
var sortedArray = insertionSort.insertsionSort(unsortedArray);
const endTime = process.hrtime.bigint();

// # CHANGE ABOVE THIS COMMENT #
console.log("# Sorting has ended #");

var totalTime = endTime - startTime;
console.log("Take this time and convert to milliseconds");
console.log(totalTime);