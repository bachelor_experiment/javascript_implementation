var fileSystem = require('fs');

const PathToTestData = "/home/casperbjork/Development/Bachelor_Thesis/testData/";

exports.readTestData = (fileName) => {
    var resultArray = fileSystem.readFileSync(PathToTestData + fileName, 'utf-8');
    resultArray = resultArray.split("\n");
    resultArray.splice(resultArray.length-1, 1);
    resultArray.splice(0, 1);

    return resultArray;
}
